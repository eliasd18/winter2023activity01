import java.util.Scanner;
public class PartThree 
{
	public static void main(String[] args) 
	{
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Enter a number");
			int num1 = reader.nextInt();
		System.out.println("Enter a 2nd number");
			int num2 = reader.nextInt();
		Calculator calc = new Calculator();
		
		System.out.println("if you add them up: " + Calculator.add(num1, num2));
		System.out.println("if you substract them: " + Calculator.substract(num1, num2));
		System.out.println("if you multiply them: " + calc.multiply(num1, num2));
		System.out.println("if you divide them: " + calc.divide(num1, num2));
		
	}
}