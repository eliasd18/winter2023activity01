public class MethodsTest 
{
	public static void main(String[] args) 
	{
		int x = 5;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
				
		System.out.println("before calling: " + x);
		methodOneInputNoReturn(x + 10);
		System.out.println("after calling: " + x);
				
		methodTwoInputNoReturn(10, 15.2);
				
		int z = methodNoInputReturnInt();
		System.out.println(z);
			
		System.out.println(sumSquareRoot(9, 5));
		
		String s1 = "java";
		String s2 = "programming";
		System.out.println("s1 length is: " + s1.length());
		System.out.println("s2 length is: " + s2.length());
		
		System.out.println("using addOne method gives: " + SecondClass.addOne(50));
		
		SecondClass sc = new SecondClass();
		
		System.out.println("using addTwo method gives: " + sc.addTwo(50));

	}
	public static void methodNoInputNoReturn() 
	{
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int hello) 
	{
		hello = hello - 5;
		System.out.println("Inside the method one input no return: " + hello);
	}
	public static void methodTwoInputNoReturn(int a, double b) 
	{
		System.out.println("int is " + a + ", double is " + b);
	}
	public static int methodNoInputReturnInt() 
	{
		return 5;
	}
	public static double sumSquareRoot(int a, int b) 
	{
		double c = Math.sqrt(a + b);
		return c;
	}
}
